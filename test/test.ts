import { expect, tap } from '@pushrocks/tapbundle';
import * as smartcrypto from '../ts/index.js';

let testSmartCrypto: smartcrypto.Smartcrypto;

tap.test('first test', async () => {
  testSmartCrypto = new smartcrypto.Smartcrypto();
  expect(testSmartCrypto).toBeInstanceOf(smartcrypto.Smartcrypto);
});

tap.test('should create a private public keypair', async () => {
  const keypair = await testSmartCrypto.createKeyPair();
  console.log(keypair);
})

tap.start();
