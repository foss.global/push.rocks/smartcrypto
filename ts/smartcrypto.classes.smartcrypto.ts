import * as plugins from './smartcrypto.plugins.js';
import { KeyPair } from './smartcrypto.classes.keypair.js';

export class Smartcrypto {
  public async createKeyPair(): Promise<KeyPair> {
    return KeyPair.createNewKeyPair();
  }
}
