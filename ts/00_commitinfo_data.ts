/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartcrypto',
  version: '2.0.1',
  description: 'easy crypto methods'
}
