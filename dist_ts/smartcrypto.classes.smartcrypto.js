"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Smartcrypto = void 0;
const smartcrypto_classes_keypair_1 = require("./smartcrypto.classes.keypair");
class Smartcrypto {
    async createKeyPair() {
        return smartcrypto_classes_keypair_1.KeyPair.createNewKeyPair();
    }
}
exports.Smartcrypto = Smartcrypto;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic21hcnRjcnlwdG8uY2xhc3Nlcy5zbWFydGNyeXB0by5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uL3RzL3NtYXJ0Y3J5cHRvLmNsYXNzZXMuc21hcnRjcnlwdG8udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQ0EsK0VBQXdEO0FBRXhELE1BQWEsV0FBVztJQUNmLEtBQUssQ0FBQyxhQUFhO1FBQ3hCLE9BQU8scUNBQU8sQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0lBQ3BDLENBQUM7Q0FDRjtBQUpELGtDQUlDIn0=