import { KeyPair } from './smartcrypto.classes.keypair';
export declare class Smartcrypto {
    createKeyPair(): Promise<KeyPair>;
}
